import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable()
export class EditTicketService {

  public isShowEditTicketModal$ = new BehaviorSubject<boolean>(false);

  constructor() {}

  public setIsShowEditTicketModal(val: boolean): void {
    this.isShowEditTicketModal$.next(val);
  }
}
