import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from "rxjs";
import {TicketsFacade} from "../../../state";
import {ITicket} from "@shared/models/ticket.model";
import {takeUntil} from "rxjs/operators";

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject();

  public tickets: ITicket[] = [];

  constructor(private ticketsFacade: TicketsFacade) {}

  ngOnInit(): void {
    this.getTickets();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private getTickets(): void {
    this.ticketsFacade.getTickets();
    this.ticketsFacade.$tickets
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe((res: ITicket[]) => this.tickets = res);
  }
}
