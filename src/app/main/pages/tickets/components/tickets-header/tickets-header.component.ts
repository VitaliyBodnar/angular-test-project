import {Component} from '@angular/core';
import {EditTicketService} from "../../services/edit-ticket-service/edit-ticket.service";

@Component({
  selector: 'app-tickets-header',
  templateUrl: './tickets-header.component.html',
  styleUrls: ['./tickets-header.component.scss']
})
export class TicketsHeaderComponent {

  constructor(private editTicketService: EditTicketService) {
  }

  public openEditTicketDialog(): void {
    this.editTicketService.setIsShowEditTicketModal(true);
  }
}
