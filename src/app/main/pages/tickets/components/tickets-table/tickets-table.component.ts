import {AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {ITicket, TicketStatus} from "@shared/models/ticket.model";
import {DepartmentsName} from "@shared/models/departments.model";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {TicketEditDialogComponent} from "../ticket-edit-dialog/ticket-edit-dialog.component";
import {EditTicketService} from "../../services/edit-ticket-service/edit-ticket.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {MatSort} from "@angular/material/sort";
import {ComponentType} from "@angular/cdk/overlay";

@Component({
  selector: 'app-tickets-table',
  templateUrl: './tickets-table.component.html',
  styleUrls: ['./tickets-table.component.scss']
})
export class TicketsTableComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  @Input() tickets: ITicket[] = [];

  private destroy$: Subject<void> = new Subject();

  public readonly ticketsColumns: string[] = [
    'identifier',
    'createdAt',
    'updatedAt',
    'departmentIdentifier',
    'title',
    'status',
    'details'
  ];

  public readonly dialogConfig: MatDialogConfig = {
    width: '500px',
    minHeight: '740px',
    maxHeight: '807px'
  }

  public readonly departmentsName: { [key: string]: string } = DepartmentsName;
  public readonly ticketStatus = TicketStatus;

  public dataSource: MatTableDataSource<ITicket> = new MatTableDataSource<ITicket>(this.tickets);

  constructor(
    private editTicketService: EditTicketService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.getIsShowEditTicketModal();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('tickets' in changes) {
      this.refreshDataSource();
    }
  }

  ngAfterViewInit(): void {
    this.refreshDataSource();
    this.setDataSourceOptions();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private refreshDataSource(): void {
    this.dataSource = new MatTableDataSource<ITicket>(this.tickets);
  }

  private setDataSourceOptions(): void {
    if (!this.dataSource) {
      return;
    }
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  private getIsShowEditTicketModal(): void {
    this.editTicketService.isShowEditTicketModal$
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe((val: boolean) => val && this.openEditDialog());
  }

  public openEditDialog(ticket?: ITicket): void {
    this.dialog.open(TicketEditDialogComponent as ComponentType<TicketEditDialogComponent>,
      {
        ...this.dialogConfig,
        data: ticket,
      }
    );
  }

  public copyToClipboard(text: string) {
    navigator.clipboard.writeText(text);
  }
}
