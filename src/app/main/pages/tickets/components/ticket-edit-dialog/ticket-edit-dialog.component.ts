import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DepartmentDropdownOptions} from "@shared/models/departments.model";
import {ITicket} from "@shared/models/ticket.model";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-ticket-edit-dialog',
  templateUrl: './ticket-edit-dialog.component.html',
  styleUrls: ['./ticket-edit-dialog.component.scss']
})
export class TicketEditDialogComponent implements OnInit {

  public ticket: ITicket;
  public form: FormGroup | undefined;

  public departmentDropdownOptions = DepartmentDropdownOptions;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<TicketEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.ticket = data;
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      title: '',
      departmentIdentifier: '',
      problem: '',
      attachments: []
    })
    this.ticket && this.form.patchValue(this.ticket);
  }
}
