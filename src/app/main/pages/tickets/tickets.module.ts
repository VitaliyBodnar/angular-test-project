// Material
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from '@angular/material/icon';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSortModule} from "@angular/material/sort";

// Common
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TicketsComponent} from './tickets.component';
import {TicketsRoutingModule} from "./tickets-routing.module";
import {TicketsTableComponent} from './components/tickets-table/tickets-table.component';
import {TicketsHeaderComponent} from './components/tickets-header/tickets-header.component';
import {TicketEditDialogComponent} from './components/ticket-edit-dialog/ticket-edit-dialog.component';
import {EditTicketService} from "./services/edit-ticket-service/edit-ticket.service";
import {SharedModule} from "@shared/shared.module";

const materialImports = [
  MatTableModule,
  MatButtonModule,
  MatIconModule,
  MatPaginatorModule,
  MatDialogModule,
  MatSortModule
];

@NgModule({
  declarations: [
    TicketsComponent,
    TicketsTableComponent,
    TicketsHeaderComponent,
    TicketEditDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TicketsRoutingModule,
    ...materialImports,
  ],
  providers: [EditTicketService]
})
export class TicketsModule {}
