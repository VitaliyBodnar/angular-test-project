import {ActionReducerMap} from "@ngrx/store";
import {AppState} from "./state";
import {ticketsReducer} from "./tickets/tickets.reducer";

export const reducers: ActionReducerMap<AppState> = {
    tickets: ticketsReducer,
};
