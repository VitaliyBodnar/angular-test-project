import {TicketsState} from "./tickets/tickets.state";

export interface AppState {
    tickets: TicketsState;
}
