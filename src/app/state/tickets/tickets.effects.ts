import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, switchMap} from 'rxjs/operators';
import * as ticketsActions from "./tickets.actions";
import {TicketsService} from "@shared/services/tickets-service/tickets.service";
import {TicketsActionTypes} from "./tickets.actions";
import {ITicket} from "@shared/models/ticket.model";

@Injectable()
export class TicketsEffects {

    public readonly getTickets$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(TicketsActionTypes.GetTickets),
            switchMap(() => this.ticketsService.getTickets()),
            map((data: ITicket[]) => ticketsActions.GetTicketsSuccess({data}))
        );
    });

    constructor(
        private actions$: Actions,
        private ticketsService: TicketsService
    ) {
    }
}
