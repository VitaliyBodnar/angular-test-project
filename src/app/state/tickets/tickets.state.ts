import {ITicket} from "@shared/models/ticket.model";

export interface TicketsState {
    tickets: ITicket[];
    error?: string | null;
}

export const initialTicketState: TicketsState = {
    tickets: [] as ITicket[],
    error: null,
};
