import {createAction, props} from '@ngrx/store';
import {ITicket} from "@shared/models/ticket.model";

export enum TicketsActionTypes {
    GetTickets = '[tickets] Get tickets',
    GetTicketsSuccess = '[tickets] Get tickets success',
}

export const GetTickets = createAction(
  TicketsActionTypes.GetTickets
);

export const GetTicketsSuccess = createAction(
  TicketsActionTypes.GetTicketsSuccess,
    props<{ data: ITicket[] }>()
);

