import {initialTicketState, TicketsState} from "./tickets.state";
import {Action, createReducer, on} from "@ngrx/store";
import * as ticketActions from './tickets.actions';

export const reducer = createReducer(
    initialTicketState,
    on(ticketActions.GetTicketsSuccess, (state, { data }) => ({
      ...state,
      tickets: data
    })),
);

export function ticketsReducer(state: TicketsState | undefined, action: Action) {
  return reducer(state, action);
}
