import {Injectable} from '@angular/core';

import {select, Store} from '@ngrx/store';
import {Observable} from "rxjs";
import {AppState} from "../state";
import {ITicket} from "@shared/models/ticket.model";
import * as ticketsActions from './tickets.actions';
import * as ticketsSelectors from './tickets.selectors';

@Injectable({
    providedIn: 'root'
})
export class TicketsFacade {
    public $tickets: Observable<ITicket[]> = this.store.pipe(select(ticketsSelectors.getTickets));

    constructor(private store: Store<AppState>) {}

    public getTickets(): void {
        this.store.dispatch(ticketsActions.GetTickets());
    }
}
