import {AppState} from "../state";
import {createSelector} from "@ngrx/store";

export const getTicketState = (state: AppState) => state.tickets;

export const getTickets = createSelector(
    getTicketState,
    state => {
      return state.tickets;
    }
);
