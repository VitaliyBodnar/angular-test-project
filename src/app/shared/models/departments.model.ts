import {IDropdownOption} from "@shared/models/dropdown.model";

export enum DepartmentIdentifier {
  SalesDepartment,
  FinancialDepartment,
  TechnicalDepartment,
  SecurityDepartment,
  WithdrawalDepartment,
  AccountVerificationDepartment,
  ComplianceRiskDepartment,
  ProjectVerificationDepartment
}

export const DepartmentsName = {
  [DepartmentIdentifier.SalesDepartment]: 'Sales department',
  [DepartmentIdentifier.FinancialDepartment]: 'Financial Department',
  [DepartmentIdentifier.TechnicalDepartment]: 'Technical Department',
  [DepartmentIdentifier.SecurityDepartment]: 'Security Department',
  [DepartmentIdentifier.WithdrawalDepartment]: 'Withdrawal Department',
  [DepartmentIdentifier.AccountVerificationDepartment]: 'Account Verification Department',
  [DepartmentIdentifier.ComplianceRiskDepartment]: 'Compliance/risk Department',
  [DepartmentIdentifier.ProjectVerificationDepartment]: 'Project Verification Department',
}

export const DepartmentDropdownOptions: IDropdownOption[] = [
  {
    label: DepartmentsName[DepartmentIdentifier.SalesDepartment],
    value: DepartmentIdentifier.SalesDepartment
  },
  {
    label: DepartmentsName[DepartmentIdentifier.FinancialDepartment],
    value: DepartmentIdentifier.FinancialDepartment
  },
  {
    label: DepartmentsName[DepartmentIdentifier.TechnicalDepartment],
    value: DepartmentIdentifier.TechnicalDepartment
  },
  {
    label: DepartmentsName[DepartmentIdentifier.SecurityDepartment],
    value: DepartmentIdentifier.SecurityDepartment
  },
  {
    label: DepartmentsName[DepartmentIdentifier.WithdrawalDepartment],
    value: DepartmentIdentifier.WithdrawalDepartment
  },
  {
    label: DepartmentsName[DepartmentIdentifier.AccountVerificationDepartment],
    value: DepartmentIdentifier.AccountVerificationDepartment
  },
  {
    label: DepartmentsName[DepartmentIdentifier.ComplianceRiskDepartment],
    value: DepartmentIdentifier.ComplianceRiskDepartment
  },
  {
    label: DepartmentsName[DepartmentIdentifier.ProjectVerificationDepartment],
    value: DepartmentIdentifier.ProjectVerificationDepartment
  },
];
