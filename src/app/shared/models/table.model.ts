export interface ITableSearchOptions {
  items: any[];
  fieldsToSearchBy: string[];
  searchValue: string;
}

export interface ITableFilterOption {
  callback: (item: any, options: any) => boolean;
  options?: any;
}
