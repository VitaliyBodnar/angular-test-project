import { DepartmentIdentifier} from "@shared/models/departments.model";

export enum TicketStatus {
  New,
  Edited
}

export interface ITicket {
  identifier: string;
  departmentIdentifier: DepartmentIdentifier;
  title: string;
  messages: string;
  attachments?: File[];
  status: TicketStatus;
  createdAt: number;
  updatedAt: number;
}
