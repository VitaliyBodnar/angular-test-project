import {Component, forwardRef, OnDestroy, OnInit} from '@angular/core';
import {Editor, Toolbar} from "ngx-editor";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextEditorComponent),
      multi: true,
    }
  ]
})
export class TextEditorComponent implements OnDestroy, ControlValueAccessor {

  public editor: Editor = new Editor();
  public toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']}],
  ];

  public value: string = '';

  private onTouched = () => {};
  public onChange = (value: string) => {};

  ngOnDestroy(): void {
    this.editor.destroy();
  }

  writeValue(val: any): void {
    this.value = val;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  registerOnChange(fn: any): void {
    this.onChange = (value) => {
      fn(value);
    };
  }
}
