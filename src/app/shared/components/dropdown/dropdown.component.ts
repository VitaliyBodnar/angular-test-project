import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {IDropdownOption} from "@shared/models/dropdown.model";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownComponent),
      multi: true,
    }
  ]
})
export class DropdownComponent implements ControlValueAccessor {

  @Input() options!: IDropdownOption[];
  @Input() placeholder: string = '';

  public selectedValue: IDropdownOption | undefined;
  public value: IDropdownOption | undefined;

  public onChange = (value: IDropdownOption) => {};
  private onTouched = () => {};

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public registerOnChange(fn: any): void {
    this.onChange = (value) => {
      fn(value);
    };
  }

  public writeValue(val: any): void {
    this.value = val;
    this.selectedValue = val;
  }
}
