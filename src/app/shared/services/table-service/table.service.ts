import { Injectable } from '@angular/core';
import {ITableFilterOption, ITableSearchOptions} from "@shared/models/table.model";

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor() { }

  public searchItems(options: ITableSearchOptions): any[] {
    const {items, searchValue} = options;
    if (!searchValue) {
      return items;
    }
    return items.filter(item => this.searchByFields(item, options));
  }

  public searchByFields(item: any, options: ITableSearchOptions): boolean {
    const { fieldsToSearchBy, searchValue } = options;
    let matching = false;

    for (const field of fieldsToSearchBy) {
      const value = `${ item[field] }`.toLocaleLowerCase();

      if (value.includes(searchValue.toLocaleLowerCase())) {
        matching = true;
        break;
      }
    }
    return matching;
  }

  public filterItems(items: any[], filterOptions: ITableFilterOption[], additionalFilterOptions: ITableFilterOption[] = []): any[] {

    return items.filter(item => {
      return filterOptions.every(filterOption => filterOption.callback(item, filterOption.options)) &&
        (
          !additionalFilterOptions.length
          || additionalFilterOptions.some(filterOption => filterOption.callback(item, filterOption.options))
        );
    });
  }
}
