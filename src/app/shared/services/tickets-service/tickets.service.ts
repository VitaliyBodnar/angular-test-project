import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {tickets} from "../../../../assets/data/tickets";
import {ITicket} from "@shared/models/ticket.model";

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  constructor() { }

  public getTickets(): Observable<ITicket[]> {
    return of(tickets);
  }
}
