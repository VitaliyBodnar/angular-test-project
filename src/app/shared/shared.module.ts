// Material
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputComponent} from './components/input/input.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DropdownComponent} from './components/dropdown/dropdown.component';
import {TextEditorComponent} from './components/text-editor/text-editor.component';
import {NgxEditorModule} from "ngx-editor";

const declarationsToExport = [
  InputComponent,
  DropdownComponent,
  TextEditorComponent
];

const importsToExport = [
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  FormsModule,
  ReactiveFormsModule,
  NgxEditorModule
];

@NgModule({
  declarations: [
    ...declarationsToExport,
  ],
  imports: [
    CommonModule,
    ...importsToExport,
  ],
  exports: [
    ...declarationsToExport,
    ...importsToExport
  ]
})
export class SharedModule {}
